package com.uniquelist;

import junit.framework.Test;

import java.util.*;

/**
 * Created by accord on 22.02.15.
 *
 */

public class Processor {
	
	public static final Integer[] INDEXES_TO_GET = {5, 7};

	private TestObject[] arrObjects;
	
	public Processor(TestObject[] arrObjects) {
		this.arrObjects = arrObjects;
	}

	public List<TestObject> getUniqueList() {
		
		Set<Integer> indexes = getIndexesValuesToGetFromArray();
		
		return fillResultList(indexes);
		
	}
	
	private Set<Integer> getIndexesValuesToGetFromArray() {
		
		Set<Integer> allIndexes = new HashSet<Integer>();
		
		for (Integer indexToGet : INDEXES_TO_GET) {
			int indexesCount = getMaxIndexesCount(indexToGet);
			for (int i = 1; i <= indexesCount; i++) {
				allIndexes.add(i * indexToGet);
			}
		}
		
		return allIndexes;
	}
	
	private int getMaxIndexesCount(int numberToDivide) {
		return arrObjects.length / numberToDivide;
	}
	
	private List<TestObject> fillResultList(Set<Integer> indexes) {

        Set<TestObject> resultSet = new TreeSet<>(new Comparator<TestObject>() {
            @Override
            public int compare(TestObject o1, TestObject o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });

		Iterator<Integer> iter = indexes.iterator();
		
		while (iter.hasNext()) {
			int indexInArr = iter.next() - 1;
            resultSet.add(arrObjects[indexInArr]);
		}

        List<TestObject> resultsList = new ArrayList<TestObject>();
        resultsList.addAll(resultSet);
		return resultsList;
	}
	
}
