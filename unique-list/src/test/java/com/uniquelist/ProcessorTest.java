package com.uniquelist;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by accord on 22.02.15.
 *
 */

public class ProcessorTest {
	
	private Processor processor;
	
	@Before
	public void setUp() {

        TestObject[] testObjects = new TestObject[50];
        for (int i = 1; i <= 25; i++) {
            testObjects[i-1] = new TestObject(i);
        }
        for (int i = 26; i <= 50; i++) {
            testObjects[i-1] = new TestObject(i - 25);
        }
		processor = new Processor(testObjects);
	}
	
	@Test
	public void expectRightListResponse() {
        TestObject[] expectedArr = new TestObject[] {
                new TestObject(3),
                new TestObject(5),
                new TestObject(7),
                new TestObject(10),
                new TestObject(14),
                new TestObject(15),
                new TestObject(17),
                new TestObject(20),
                new TestObject(21),
                new TestObject(24),
                new TestObject(25)
        };
		List<TestObject> expectedList = Arrays.asList(expectedArr);

		assertEquals(expectedList, processor.getUniqueList());
	}
	
}
