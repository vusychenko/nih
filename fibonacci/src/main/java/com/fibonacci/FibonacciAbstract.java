package com.fibonacci;

import java.util.List;

/**
 * Created by accord on 22.02.15.
 *
 */

public abstract class FibonacciAbstract {
	
	protected int fibonacciNumber;
	
	public FibonacciAbstract(int fibonacciNumber) {
		this.fibonacciNumber = fibonacciNumber;
	}
	
	public abstract void process();
	
	protected void printFibonacciList(List<Long> resultList) {
		for (Long fibonacci : resultList) {
            System.out.print(fibonacci + " ");
        }
		System.out.println();
	}
	
}
