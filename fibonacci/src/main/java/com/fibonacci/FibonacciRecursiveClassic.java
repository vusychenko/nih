package com.fibonacci;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by accord on 22.02.15.
 *
 */

public class FibonacciRecursiveClassic extends FibonacciAbstract {
	
	public FibonacciRecursiveClassic(int fibonacciNumber) {
		super(fibonacciNumber);
	}

	public void process() {
		System.out.println("Fibonacci recursive classic function: ");
		
		List<Long> resultsFibonacciList = new ArrayList<Long>();
		
		for (int i = 0; i <= this.fibonacciNumber; i++) {
			resultsFibonacciList.add(recursiveFibFunct(i));
        }
		
		printFibonacciList(resultsFibonacciList);
		
	}
	
	private long recursiveFibFunct(int n) {    	
        if (n <= 1) {
        	return n;
        }
        else {
        	return recursiveFibFunct(n - 1) + recursiveFibFunct(n - 2);
        }
    }
	
}
