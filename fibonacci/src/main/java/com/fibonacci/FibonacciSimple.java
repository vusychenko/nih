package com.fibonacci;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by accord on 22.02.15.
 *
 */

public class FibonacciSimple extends FibonacciAbstract {

	public FibonacciSimple(int fibonacciNumber) {
		super(fibonacciNumber);
	}

	public void process() {
		
		System.out.println("Fibonacci simple: ");
		
		List<Long> resultsFibonacciList = new ArrayList<Long>();
		
		if (this.fibonacciNumber >= 1) {
			resultsFibonacciList.add(0l);
			resultsFibonacciList.add(1l);
			if (this.fibonacciNumber >= 2) {
				resultsFibonacciList.add(1l);
			}
		}					

		long fibCur1 = 1, fibCur2 = 1, fibRes = 1;

		for (int i = 3; i <= this.fibonacciNumber; i++) {
			
			fibRes = fibCur1 + fibCur2;
			fibCur1 = fibCur2;
			fibCur2 = fibRes;
			
			resultsFibonacciList.add(fibRes);
		}
		
		printFibonacciList(resultsFibonacciList);
		
	}

}
