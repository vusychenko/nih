package com.fibonacci;

/**
 * Created by accord on 22.02.15.
 *
 */

public class Launcher {

    public static void main(String[] args) {
        int fibonacciNumber = Integer.parseInt(args[0]);

        FibonacciAbstract fibRecursiveClassic = new FibonacciRecursiveClassic(fibonacciNumber);
        fibRecursiveClassic.process();

        FibonacciAbstract fibSimple = new FibonacciSimple(fibonacciNumber);
        fibSimple.process();

        FibonacciAbstract fibRecursiveQuick = new FibonacciRecursiveQuick(fibonacciNumber);
        fibRecursiveQuick.process();
        
    }
    
}
