package com.fibonacci;

/**
 * Created by accord on 22.02.15.
 *
 */
public class FibonacciRecursiveQuick extends FibonacciAbstract {

    public FibonacciRecursiveQuick(int fibonacciNumber) {
        super(fibonacciNumber);
    }

    @Override
    public void process() {
        System.out.println("Fibonacci recursive quick function: ");

        fibonacciRecusionQuick(0, 1, 1, this.fibonacciNumber);

    }

    private void fibonacciRecusionQuick(int numberCurrent, long currentVal1, long currentVal2, int numberMax) {
        if (numberCurrent == 0) {
            System.out.print("0 ");
        } else if (numberCurrent == 1 || numberCurrent == 2) {
            System.out.print("1 ");
        } else if (numberCurrent <= numberMax) {
            long res = currentVal1  + currentVal2;
            System.out.print(res + " ");
            currentVal1 = currentVal2;
            currentVal2 = res;
        } else {
            return;
        }
        fibonacciRecusionQuick(++numberCurrent, currentVal1, currentVal2, numberMax);
    }
}
