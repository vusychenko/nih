package com.queue;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by accord on 22.02.15.
 *
 */

public class Processor<T> {

    /**
     * Processes requests from the queue with no more than
     * maxThreads threads
     * For each request object calls
     * IRequestHandler<T>.processRequest(o) only once in a separate thread
     * When the queue is empty and all processing is finished
     * no threads exist.
     *
     * @param rh - an object that handles requests
     * @param maxThreads - total number of threads
     */

    private int maxThreads;
    private IRequestHandler<T> requestHandler;

    private ConcurrentLinkedQueue<T> queue;
    private List<Thread> threadsInProgress;
    private AtomicBoolean shutdown;

    public Processor(IRequestHandler<T> requestHandler, int maxThreads) {
        this.maxThreads = maxThreads;
        this.requestHandler = requestHandler;
        this.threadsInProgress = new CopyOnWriteArrayList<Thread>();
        this.shutdown = new AtomicBoolean(false);
        Thread queuePolling = new Thread(new QueuePolling());
        queuePolling.start();
    }

    private class QueuePolling implements Runnable {

        @Override
        public void run() {
            queue = new ConcurrentLinkedQueue<T>();
            while (!shutdown.get()) {
                if (queue.size() > 0 && threadsInProgress.size() < maxThreads) {
                    Executor executor = new Executor(queue.poll());
                    Thread threadExecutor = new Thread(executor);
                    threadsInProgress.add(threadExecutor);
                    threadExecutor.start();
                }
            }
        }
    }

    private class Executor implements Runnable {

        private T obj;

        public Executor(T obj) {
            this.obj = obj;
        }

        @Override
        public void run() {
            try {
                requestHandler.processRequests(obj);
            } catch (InterruptedException e) {
                // force shutdown
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                threadsInProgress.remove(this);
            }
        }
    }

    /**
     * Puts the request into a queue, does not wait
     * for the request to complete
     * @param o - request object
     */
    public void addRequest(T o) {
        if (!this.shutdown.get()) {
            queue.add(o);
        }
    }

    /** OPTIONAL
     * Asynchronous shutdown, returns immediately.
     * Instructs the processor to stop accepting requests
     * and finish existing tasks
     * @param o – if not null, notifies all waiting threads on
     * this object upon successful shutdown
     */
    public void shutDown(Object o) {
        this.shutdown.set(true);
        for (Thread thread : threadsInProgress) {
            if (thread.isAlive()) {
                thread.interrupt();
            }
        }
        if (o != null) {
            o.notifyAll();
        }
    }
    /** OPTIONAL
     * @returns true if the processor is shut down
     */
    public boolean isShutDown() {
        return this.shutdown.get();
    }
}
